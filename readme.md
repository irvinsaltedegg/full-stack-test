## Frontend Test Project

This test is divided into *TWO* mandatory challenge and 4 optional challenges.

In order to submit the project, you MUST complete **Challenge 1** & **Challenge 2**.
For every optional challenge completed, you will earn score that will be summed into your final score.

We will select candidates who gets the highest score
_(You do not need to complete all)_

### Submission
Submission can be done in TWO ways:

1. Send zipped project files to yohanes@irvinsaltedegg.com
2. **EXTRA 50 POINT**: Upload it to Github and email Github link to yohanes@irvinsaltedegg.com

---

## Challenge 1 (MANDATORY)
**Build simple REST API endpoint to mirror https://kommercio.id/full-stack-test/products.json**
- Please use any of these backend languages: PHP (Preferable Laravel) or Node.js (Preferably Express)
- No database connection is needed.

## Challenge 2 (MANDATORY)
**Build the mockup website using modern Javascript framework like Vue.js / React / Angular**
- Please find the mockup design in `mockup` folder
- Consume the API you created for Challenge 1

---

### OPTIONAL CHALLENGES

### Challenge 3 (Max score: 100)
Use **SASS** or **Less** CSS pre-processors

### Challenge 4 (Max score: 100)
Build this project with **Webpack**.

### Challenge 5 (Max score: 200)
- Add unit-testing to Challenge 1 (Max score: 100)
- Add unit-testing to Challenge 2 (Max score: 100)

### Challenge 6 (Max score: 50)
Submit your project on Github
